from logger.logger_base import log

# This class just raise exceptions while accessing data in the database
class DeviceService:
    def __init__(self, db_connector):
        self.db_connector = db_connector

    # Service for GET request
    def get_all_devices(self):
        try:
            devices = list(self.db_connector.db.devices.find())
            return devices
        except Exception as e:
            log.critical(f'Error fetching all devices from database: {e}')
            raise

    # Service for GET BY ID request
    def get_device_by_id(self, device_id):
        try:
            device = self.db_connector.db.devices.find_one({'_id': device_id})
            return device
        except Exception as e:
            log.critical(f'Error fetching device with id {device_id} from the database: {e}')
            raise

    # Service for POST request (can't raise exception)
    def add_device(self, new_device):
        # Sort by id and get the last document
        latest_device = self.db_connector.db.devices.find_one({}, sort=[('_id', -1)])
        new_id = (latest_device['_id'] + 1) if latest_device else 1
        new_device['_id'] = new_id
        self.db_connector.db.devices.insert_one(new_device)

        # Returns added register
        return new_device
        

    # Service for PUT request
    def update_device(self, device_id, updated_data):
        try:
            device = self.db_connector.db.devices.find_one({'_id': device_id})
            if not device:
                return None
            
            self.db_connector.db.devices.update_one({"_id": device_id}, {"$set": updated_data})

            # Returns updated register for device_id
            return self.db_connector.db.devices.find_one({'_id': device_id})
        except Exception as e:
            log.critical(f'Error updating device with id {device_id} in the database: {e}')
            raise

    # Service for DELETE request
    def delete_device(self, device_id):
        try:
            device = self.db_connector.db.devices.find_one({'_id': device_id})
            if not device:
                return None
            
            self.db_connector.db.devices.delete_one({"_id": device_id})
            
            # Returns deleted register which matches with device_id
            return device
        except Exception as e:
            log.critical(f'Error deleting device with id {device_id} from the database: {e}')
            raise
