from flask import Flask
from models.device_models import DeviceModel
from services.device_services import DeviceService 
from routes.device_routes import DeviceRoutes
from schemas.device_schema import DeviceSchema
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)

# Configures swagger...
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
swagger_ui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': 'Platforms API'
    }
)
# Enables route for swagger docs
app.register_blueprint(swagger_ui_blueprint)


# Create connection to the database 
db_connector = DeviceModel()
db_connector.connect_to_database()

# Associate the services with the data on the database
device_service = DeviceService(db_connector)

# Create validation instance
device_schema = DeviceSchema()

# Associate routes with the services
device_blueprint = DeviceRoutes(device_service, device_schema)
app.register_blueprint(device_blueprint)

# Run application...
if __name__ == '__main__':
    try:
        app.run()
    finally:
        db_connector.close_connection()